import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/services/authentication.service';
import {FormBuilder, Validators} from '@angular/forms';
@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.css']
})
export class InicioComponent implements OnInit {
  panelOpenState = false;
  constructor(   private _formBuilder: FormBuilder) { }

  ngOnInit(): void {
  }
  primeroFormGroup = this._formBuilder.group({
    primeroCtrl: ['', Validators.required],
  });
  segundoFormGroup = this._formBuilder.group({
    segundoCtrl: ['', Validators.required],
  });
  terceroFormGroup = this._formBuilder.group({
    tercerCtrl: ['', Validators.required],
  });
  cuartoFormGroup = this._formBuilder.group({
    cuartoCtrl: ['', Validators.required],
  });
  quintoFormGroup = this._formBuilder.group({
    quintoCtrl: ['', Validators.required],
  });
}
