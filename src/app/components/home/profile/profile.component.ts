import { Component, OnInit } from '@angular/core';
import { User } from '@angular/fire/auth';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { HotToastService } from '@ngneat/hot-toast';
import { ImageUploadService } from 'src/app/services/image-upload.service';

import { concatMap} from 'rxjs';
import { FormControl, FormGroup } from '@angular/forms';
import { UsersService } from 'src/app/services/users.service';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { ProfileUser } from 'src/app/models/user';
@UntilDestroy()
@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  user$ = this.usersService.currentUserProfile$

  profileForm = new FormGroup({
    uid:new FormControl(''),
    displayName:new FormControl(''),
    firstName:new FormControl(''),
    lastName:new FormControl(''),
    phone:new FormControl(''),
    address:new FormControl('')
  });
  
  constructor(
    private imageUploadService:ImageUploadService,
    private toast: HotToastService,
    public usersService:UsersService) { }

  ngOnInit(): void {
    this.usersService.currentUserProfile$.pipe(
      untilDestroyed(this)
    ).subscribe((user)=>{
      this.profileForm.patchValue({...user})
    })
  }


  uploadFile(event: any, user:ProfileUser) {
    this.imageUploadService
      .uploadImage(event.target.files[0], `images/profile/${user.uid}`)
      .pipe(
        this.toast.observe(
        {
          loading: 'Subiendo imagen de perfil...',
          success: 'Imagen cargada con éxito',
          error: 'Hubo un error al subir la imagen',
        }),

        concatMap((photoURL)=>this.usersService.updateUser({uid:user.uid,photoURL})
        )
      ) .subscribe();
  }

  saveProfile() {
    const profileData = this.profileForm.value;
    this.usersService
      .updateUser(profileData)
      .pipe(
        this.toast.observe({
          loading: 'Guardando datos de perfil...',
          success: 'Perfil actualizado con éxito',
          error: 'Hubo un error al actualizar el perfil',
        })
      )
      .subscribe();
  }

}
