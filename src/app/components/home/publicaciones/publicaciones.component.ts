import { Component, OnInit } from '@angular/core';
import { IPublicacion } from 'src/app/models/publicacion.interface';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { PublicacionService } from 'src/app/services/publicacion.service';
import {MatDialog} from '@angular/material/dialog';
import { AgregarPubliComponent } from '../agregar-publi/agregar-publi.component';
import { UsersService } from 'src/app/services/users.service';

@Component({
  selector: 'app-publicaciones',
  templateUrl: './publicaciones.component.html',
  styleUrls: ['./publicaciones.component.css']
})
export class PublicacionesComponent implements OnInit {
  // user$ = this.auth.currentUser$
  
  post:IPublicacion[];
  constructor(private _publicacionService:PublicacionService,
    public dialog: MatDialog,
    public usersService:UsersService,
    public auth:AuthenticationService) { 
      this.post=[{
        titulo:'prueba',
        fecha: new Date(),
        descripcion:'weknfwje wod wjeb wwlnfwjbef w'
      }]
    }


  ngOnInit(): void {
    this._publicacionService.obtenerPost().subscribe(post=>
      this.post = post)
  }

  openDialog() {
    const dialogRef = this.dialog.open(AgregarPubliComponent);
    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }

  async eliminarPost(post:IPublicacion){
    const response= this._publicacionService.deletePost(post);
    console.log(response);
    
  }
}

