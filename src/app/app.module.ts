import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SharedModule } from './components/shared/shared.module';
import { LandingComponent } from './components/landing/landing.component';
import { SignUpComponent } from './components/sign-up/sign-up.component';
import { LoginComponent } from './components/login/login.component';
import { HomeComponent } from './components/home/home.component';
import { ReactiveFormsModule } from '@angular/forms';
import { initializeApp,provideFirebaseApp } from '@angular/fire/app';
import { environment } from '../environments/environment';
import { provideAuth,getAuth } from '@angular/fire/auth';
import { HotToastModule } from '@ngneat/hot-toast';
import { PublicacionesComponent } from './components/home/publicaciones/publicaciones.component';
import { NavbarComponent } from './components/home/navbar/navbar.component';
import { AgregarPubliComponent } from './components/home/agregar-publi/agregar-publi.component';
import { ProfileComponent } from './components/home/profile/profile.component';
import { getStorage, provideStorage } from '@angular/fire/storage';
import { provideFirestore } from '@angular/fire/firestore';
import { getFirestore } from '@firebase/firestore';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { InicioComponent } from './components/home/inicio/inicio.component';


@NgModule({
  declarations: [
    AppComponent,
    LandingComponent,
    SignUpComponent,
    LoginComponent,
    HomeComponent,
    PublicacionesComponent,
    NavbarComponent,
    AgregarPubliComponent,
    ProfileComponent,
    SidebarComponent,
    InicioComponent,


  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    SharedModule,
    ReactiveFormsModule,
    provideFirebaseApp(() => initializeApp(environment.firebase)),
    provideAuth(() => getAuth()),
    provideStorage(()=>getStorage()),
    provideFirestore(()=>getFirestore()),
    HotToastModule.forRoot(),


  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
