export interface IPublicacion{
  id?:string;
  titulo:string;
  fecha:Date;
  descripcion:string;
}