import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { LandingComponent } from './components/landing/landing.component';
import { LoginComponent } from './components/login/login.component';

import { canActivate, redirectUnauthorizedTo, redirectLoggedInTo } from '@angular/fire/auth-guard';
import { PublicacionesComponent } from './components/home/publicaciones/publicaciones.component';
import { AgregarPubliComponent } from './components/home/agregar-publi/agregar-publi.component';
import { ProfileComponent } from './components/home/profile/profile.component';
import { InicioComponent } from './components/home/inicio/inicio.component';


const redirectToLogin = () => redirectUnauthorizedTo(['login']);
const redirectToHome = () => redirectLoggedInTo(['inicio']);

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: LandingComponent
  },
  {
    path: 'login',
    component: LoginComponent,
    ...canActivate(redirectToHome)
  },
  {
    path: 'sign-up',
    component: LoginComponent,
    ...canActivate(redirectToHome)
  },
  {
    path: 'home',
    component: HomeComponent,
    ...canActivate(redirectToLogin),
    children: [
      {
        path: 'inicio',
        component: InicioComponent
      },
      {
        path: 'publicaciones',
        component: PublicacionesComponent
      },
      {
        path: 'profile',
        component: ProfileComponent,
        ...canActivate(redirectToLogin),
      },
    
    ]
  },
  {
    path: 'agregar-publicacion',
    component: AgregarPubliComponent
  },
 
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
