// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  firebase: {
    projectId: 'angular-signup-cami',
    appId: '1:77697448123:web:53ac4a7e4d9b108a93919e',
    storageBucket: 'angular-signup-cami.appspot.com',
    locationId: 'us-central',
    apiKey: 'AIzaSyB2GcP30ZYG1ykHT-UpGOyLPiDO07Hp_9I',
    authDomain: 'angular-signup-cami.firebaseapp.com',
    messagingSenderId: '77697448123',
  },
  production: false
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
